from django.conf.urls import url
from django.urls import path
from guess_number.views import random_game

urlpatterns = [
    url('^$', random_game, name='random_game_url'),
]
